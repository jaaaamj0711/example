#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import re
import requests
from urllib.request import urlopen
from bs4 import BeautifulSoup


# In[2

def incruit_lv1(pages: int):
    
    assert pages > 0
    
    url_base_1 = "http://people.incruit.com/resumeguide/pdslist.asp?page="
    url_base_2 = "&listseq=1&sot=&pds1=1&pds2=11&pds3=&pds4=&schword=&rschword=&lang=&price=&occu_b_group=&occu_m_group=&occu_s_group=&career=&pass=&compty=&rank=&summary=&goodsty="
    result = pd.DataFrame()
    
    for page in range(pages):
        url = url_base_1 + str(page + 1) + url_base_2
        
        try:
            html = urlopen(url)
            soup = BeautifulSoup(html, "lxml")
            
            titles = soup.findAll("td", attrs={"style": "text-align:left"})
            titles = list(map(lambda x: x.find("a").text, titles))
            urls = soup.findAll("td", attrs={"style": "text-align:left"})
            urls = list(map(lambda x: "http://www.people.incruit.com/resumeguide" + x.find("a")["href"][1:], urls))
            
            temp = pd.DataFrame({"제목": titles, "주소": urls})
            temp = pd.DateFrame
            result = result.append(temp)
            result = result.append(temp)
            
        except:
            continue
        
    return result.reset_index(drop=True)


# %%
